import numpy as np
import astropy.io.fits as pf
import pickle
import time
import os
import matplotlib.pyplot as plt
import gc
import logging

import rainbow.plot
import rainbow.read
import rainbow.write
from rainbow.parse import read_arguments

# ignore the Deprecation Warning from the 'new' matplotlib version
import matplotlib as mlp
import warnings

warnings.filterwarnings("ignore", category=mlp.cbook.MatplotlibDeprecationWarning)
# Warning: converting a masked element to nan.
warnings.filterwarnings("ignore", category=UserWarning, module="matplotlib")
warnings.filterwarnings("ignore", category=UserWarning, module="numpy")


def three_fraction_plot(spaxels, image=None, savedir=None, show=0):
    sf_index = np.where(np.all([
        np.log10(spaxels.BPT['OIII5007/Hbeta']) < 0.61 /
        (np.log10(spaxels.BPT['NII6583/Halpha']) - 0.05) + 1.3,
        np.log10(spaxels.BPT['NII6583/Halpha']) < 0.,
    ], axis=0))[0]
    sf_spaxels = spaxels.subset(sf_index)
    mixed_index = np.where(np.all([
        np.log10(spaxels.BPT['OIII5007/Hbeta']) >= 0.61 /
        (np.log10(spaxels.BPT['NII6583/Halpha']) - 0.05) + 1.3,
        np.log10(spaxels.BPT['OIII5007/Hbeta']) < 0.61 /
        (np.log10(spaxels.BPT['NII6583/Halpha']) - 0.47) + 1.19,
    ], axis=0))[0]
    mixed_spaxels = spaxels.subset(mixed_index)
    agn_index = np.where(np.all([
        np.log10(spaxels.BPT['OIII5007/Hbeta']) >= 0.61 /
        (np.log10(spaxels.BPT['NII6583/Halpha']) - 0.47) + 1.19,
    ], axis=0))[0]
    agn_spaxels = spaxels.subset(agn_index)

    rainbow.plot.bpt(agn_spaxels, color='red', show=0)
    rainbow.plot.bpt(sf_spaxels, color='blue', size=0, show=0)
    rainbow.plot.bpt(mixed_spaxels, color='green', size=0, show=0)
    if savedir is not None:
        plt.savefig(os.path.join(savedir, '3fraction_BPT.png'), transparent=True)

    if not np.isnan(spaxels.cdelt):
        rainbow.plot.spaxel_map(agn_spaxels, color='red', show=0)
        rainbow.plot.spaxel_map(sf_spaxels, color='blue', size=0, show=0)
        rainbow.plot.spaxel_map(mixed_spaxels, color='green', size=0, show=show, image=image)
        if savedir is not None:
            plt.savefig(os.path.join(savedir, '3fraction_map.png'), transparent=True)

    plt.close('all')
    gc.collect()


def plot_status_bpt(agn_basis, sf_basis, mixed_spaxels,
                    sf_ionized_spaxels, agn_ionized_spaxels,
                    size=1, save=None, sii=False,
                    s_min=1., s_max=60., legend=0):
    all_spaxels = agn_basis.append(sf_basis).append(mixed_spaxels) \
        .append(sf_ionized_spaxels).append(agn_ionized_spaxels)

    def norm_func(array):
        return array

    norm_max = norm_func(all_spaxels.emission_lines['Halpha'].flux).max()
    norm_min = norm_func(all_spaxels.emission_lines['Halpha'].flux).min()
    dividing_lines = 1
    try:
        s = norm_func(sf_ionized_spaxels.emission_lines['Halpha'].flux)
        s_s_min = s_min + (s.min() - norm_min) * (s_max - s_min) / (norm_max - norm_min)
        s_s_max = s_max - (norm_max - s.max()) * (s_max - s_min) / (norm_max - norm_min)
        if len(s) == 1:
            s = s_s_max
        rainbow.plot.bpt(sf_ionized_spaxels, color='blue',
                         s=s,
                         s_min=s_s_min,
                         s_max=s_s_max,
                         show=0, size=size, dividing_lines=dividing_lines, label='Starforming', sii=sii)
        size = 0
        dividing_lines = 0
    except ValueError:
        pass
    try:
        s = norm_func(agn_ionized_spaxels.emission_lines['Halpha'].flux)
        s_s_min = s_min + (s.min() - norm_min) * (s_max - s_min) / (norm_max - norm_min)
        s_s_max = s_max - (norm_max - s.max()) * (s_max - s_min) / (norm_max - norm_min)
        if len(s) == 1:
            s = s_s_max
        rainbow.plot.bpt(agn_ionized_spaxels, color='orange',
                         s=s,
                         s_min=s_s_min,
                         s_max=s_s_max,
                         show=0, size=size, dividing_lines=dividing_lines, label='AGN ionized', sii=sii)
        size = 0
        dividing_lines = 0
    except ValueError:
        pass
    try:
        s = norm_func(mixed_spaxels.emission_lines['Halpha'].flux)
        s_s_min = s_min + (s.min() - norm_min) * (s_max - s_min) / (norm_max - norm_min)
        s_s_max = s_max - (norm_max - s.max()) * (s_max - s_min) / (norm_max - norm_min)
        if len(s) == 1:
            s = s_s_max
        rainbow.plot.bpt(mixed_spaxels, color='green',
                         s=s,
                         s_min=s_s_min,
                         s_max=s_s_max,
                         show=0, size=size, dividing_lines=dividing_lines, label='Mixed', sii=sii)
        size = 0
        dividing_lines = 0
    except ValueError:
        pass
    try:
        s = norm_func(agn_basis.emission_lines['Halpha'].flux)
        s_s_min = s_min + (s.min() - norm_min) * (s_max - s_min) / (norm_max - norm_min)
        s_s_max = s_max - (norm_max - s.max()) * (s_max - s_min) / (norm_max - norm_min)
        if len(s) == 1:
            s = s_s_max
        rainbow.plot.bpt(agn_basis, color='red',
                         s=s,
                         s_min=s_s_min,
                         s_max=s_s_max,
                         show=0, size=size, dividing_lines=dividing_lines, label='AGN Basis', sii=sii)
        size = 0
    except ValueError:
        pass
    plt.tight_layout()
    try:
        s = norm_func(sf_basis.emission_lines['Halpha'].flux)
        s_s_min = s_min + (s.min() - norm_min) * (s_max - s_min) / (norm_max - norm_min)
        s_s_max = s_max - (norm_max - s.max()) * (s_max - s_min) / (norm_max - norm_min)
        if len(s) == 1:
            s = s_s_max
        rainbow.plot.bpt(sf_basis, color='blueviolet',
                         s=s,
                         s_min=s_s_min,
                         s_max=s_s_max,
                         show=0, size=size, dividing_lines=dividing_lines, label='SF Basis', sii=sii,
                         legend=legend, save=save)
    except ValueError:
        if save is not None:
            plt.savefig(save, transparent=False)


def plot_status_map(agn_basis, sf_basis, mixed_spaxels,
                    sf_ionized_spaxels, agn_ionized_spaxels,
                    additional_spaxels,
                    image,
                    dividing_radius, maximum_radius, minimum_radius, agn_radius,
                    size=1, save=None):
    rainbow.plot.spaxel_map(sf_ionized_spaxels, color='blue', show=0, size=size)
    rainbow.plot.spaxel_map(additional_spaxels, color='0.4', alpha=0.5, show=0, size=0)
    rainbow.plot.spaxel_map(agn_ionized_spaxels, color='orange', show=0, size=0)
    rainbow.plot.spaxel_map(mixed_spaxels, color='green', show=0, size=0)
    rainbow.plot.spaxel_map(agn_basis, color='red', show=0, size=0)
    rainbow.plot.spaxel_map(sf_basis, color='blueviolet', show=0, size=0, image=image)
    ax = plt.gca()
    circle = plt.Circle((0., 0.), dividing_radius, color='green', fill=False)
    ax.add_artist(circle)
    circle = plt.Circle((0., 0.), maximum_radius, color='red', fill=False)
    ax.add_artist(circle)
    circle = plt.Circle((0., 0.), minimum_radius, color='k', fill=False)
    ax.add_artist(circle)
    circle = plt.Circle((0., 0.), agn_radius, color='orange', fill=False)
    ax.add_artist(circle)
    plt.tight_layout()
    if save is not None:
        plt.savefig(save)


if __name__ == '__main__':
    start_time = time.time()

    arguments = read_arguments()  # this reads your logfile
    # there will be a lot of "try: ... except AttributeError: ..." structures
    # with them I check if the certain argument was in the logfile or not

    'define datadir & outputdir'
    try:
        datadir = arguments.datadir
    except AttributeError:
        raise AttributeError('datadir argument is mandatory')
    '   set logging'
    # logging allows you to not only put the progress of the code in the terminal like print() does,
    # but also save it in a file. I find it usefull, so I leave this feature here.
    logging.basicConfig(filename=os.path.join(datadir, 'prepare_spaxels.log'),
                        level=logging.INFO, format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)

    logging.info('Data directory: {}'.format(datadir))

    try:
        outputdir = arguments.outputdir
    except AttributeError:
        outputdir = datadir
    try:
        os.makedirs(outputdir)
        logging.info(f'Output directory {outputdir} created')
    except FileExistsError:
        logging.info('Output directory: {}'.format(outputdir))


    'set image'
    # the image is used to overplot contours on a spaxel map with rainbow.plot.spaxel_map
    # this is useful to visually check if your spaxel coordinates from the table are correct
    # put "imagefile <path to file>" in the logfile
    # the imagefile should be a fits file with a flat image (not a cube)
    # if "datacube <path to file>" is specified in the logfile, then a whitelight image is created
    image_time = time.time()
    try:
        imagefile = arguments.imagefile
        with pf.open(datadir + imagefile) as galaxy:
            image = galaxy[0].data
            image[image <= 0.] = np.nan
        logging.info('Image read {:.3f} min'.format((time.time() - image_time) / 60.))
    except AttributeError:
        image = None
    if image is None:
        try:
            datacube = arguments.datacube
            with pf.open(datadir + datacube) as galaxy:
                image = galaxy[1].data.sum(axis=0)
                image[image <= 0.] = np.nan
            imagefile = datacube.replace('.fits.gz', '_whitelight.fits')  # if datacube ends with '.fits'
                                                                          # change '.fits.gz' to '.fits'
            rainbow.write.fits_image(image, outputfile=datadir + imagefile)  # the created image is saved
            with open(arguments.logfile, 'a') as logfile:
                logfile.write(f'imagefile {imagefile}\n')  # the line is added to the logfile
            logging.info('Whitelight image created {:.3f} min'.format((time.time() - image_time) / 60.))
        except AttributeError:
            logging.warning('No image')

    'read data'
    read_time = time.time()
    data_read = False

    # the 'pickle' mechanichs will be explained further
    if arguments.pickle:
        try:
            logging.info('Reading pickled objects')
            all_spaxels = pickle.load(open(outputdir + 'all_spaxels.p', 'rb'))
            data_read = True
            logging.info('done {:.4f} min'.format((time.time() - read_time) / 60.))
        except FileNotFoundError:
            data_read = False

    # the fits table is read here:
    if not data_read:
        print('Starting to read:')
        try:
            line_names = arguments.lines.split(',')
        except AttributeError:
            line_names = ['Halpha', 'Hbeta', 'OIII5007', 'NII6583']
            with open(arguments.logfile, 'a') as logfile:
                logfile.write(f'lines {",".join(line_names)}\n')

        inputfile = arguments.inputfile
        print('reading not binned file {};'.format(inputfile))
        try:
            not_binned = int(arguments.not_binned)
        except AttributeError:
            not_binned = 1
        all_spaxels = rainbow.read.fits_table(datadir + inputfile, line_names, binning=not_binned)

        # the all_spaxels object is defined now and I save it as a class instance using the pickle package
        # pickled objects are very fast to operate with
        # if there is an 'all_spaxels.p' saved already,
        # you can run the code with the additional `-p`: 'python prepare_spaxels.py --logfile ... -p'
        # then the table will not be read again, but the pickled object is used instead
        with open(outputdir + 'all_spaxels.p', 'wb') as f:
            pickle.dump(all_spaxels, f)
        logging.info('done {:.3f} min'.format((time.time() - read_time) / 60.))

    'read spatial info'
    # this part tries to read the spatial information from the logfile:
    # (nx, ny - max number of pixels along the axis;
    #  xc, yc - the coordinates of the object's center
    #  cdelt - needs to be specified so that rainbow.plot.spaxel_map works);
    # and adds the arguments with the defaults to the logfile
    # the defaults can be then adjusted
    try:
        nx = int(arguments.nx)
        ny = int(arguments.ny)
        try:
            xc = float(arguments.xc)
            yc = float(arguments.yc)
        except AttributeError:
            xc = nx / 2.
            yc = ny / 2.
    except AttributeError:
        try:
            qso_file = arguments.QSOfile  # QSO model cube from QDeblend is very robust to automaticaly set
                                          # the center of the object
            with pf.open(datadir + qso_file) as QSOhdu:
                qso = QSOhdu[0].data.sum(axis=0)
                yc, xc = np.where(qso == qso.max())[0][0], np.where(qso == qso.max())[1][0]
                ny, nx = qso.shape
            try:
                xc = float(arguments.xc)
                yc = float(arguments.yc)
                with open(arguments.logfile, 'a') as logfile:
                    logfile.write(f'nx {nx}\n')
                    logfile.write(f'ny {ny}\n')
            except AttributeError:
                with open(arguments.logfile, 'a') as logfile:
                    logfile.write(f'xc {xc}\n')
                    logfile.write(f'yc {yc}\n')
                    logfile.write(f'nx {nx}\n')
                    logfile.write(f'ny {ny}\n')
        except AttributeError:
            nx = np.nan
            ny = np.nan
            try:
                xc = float(arguments.xc)
                yc = float(arguments.yc)
            except AttributeError:
                xc = np.nan
                yc = np.nan
            with open(arguments.logfile, 'a') as logfile:
                logfile.write(f'nx {0}\n')
                logfile.write(f'ny {0}\n')
                logfile.write(f'xc {0}\n')
                logfile.write(f'yc {0}\n')
    try:
        cdelt = float(arguments.cdelt)
    except AttributeError:
        cdelt = np.nan
        logging.warning('Cdelt is nan')
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'cdelt {cdelt}\n')

    # this is needed for the BPT morphology if the BPT is complex
    try:
        dividing_radius = float(arguments.dividing_radius)
    except AttributeError:
        dividing_radius = np.inf
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'dividing_radius {dividing_radius}\n')
    try:
        maximum_radius = float(arguments.maximum_radius)
    except AttributeError:
        maximum_radius = np.inf
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'maximum_radius {maximum_radius}\n')
    try:
        minimum_radius = float(arguments.minimum_radius)
    except AttributeError:
        minimum_radius = 0.
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'minimum_radius {minimum_radius}\n')
    try:
        agn_radius = float(arguments.agn_radius)
    except AttributeError:
        agn_radius = np.inf
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'agn_radius {agn_radius}\n')

    'read bpt info'
    # these arguments define the AGN basis region on the BPT
    try:
        agn_min_bpt_x = float(arguments.agn_min_bpt_x)
    except AttributeError:
        agn_min_bpt_x = -0.4
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'agn_min_bpt_x {agn_min_bpt_x}\n')
    try:
        agn_max_bpt_y = float(arguments.agn_max_bpt_y)
    except AttributeError:
        agn_max_bpt_y = np.inf
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'agn_max_bpt_y {agn_max_bpt_y}\n')
    try:
        agn_min_bpt_y = float(arguments.agn_min_bpt_y)
    except AttributeError:
        agn_min_bpt_y = 0.75
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'agn_min_bpt_y {agn_min_bpt_y}\n')
    # these arguments set the minimum SNR for the AGN and SF bases
    try:
        min_sf_basis_snr = float(arguments.min_sf_basis_snr)
    except AttributeError:
        min_sf_basis_snr = 3.
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'min_sf_basis_snr {min_sf_basis_snr}\n')
    try:
        min_agn_basis_snr = float(arguments.min_agn_basis_snr)
    except AttributeError:
        min_agn_basis_snr = 3.
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'min_agn_basis_snr {min_agn_basis_snr}\n')
    # now all the necessary arguments are read
    # if the arguments were not specified, the default values are added to the logfile

    'initialize spaxels'
    all_spaxels.set_spatial_info(nx, ny, cdelt, xc, yc)
    all_spaxels.correct_extinction()
    all_spaxels.set_bpt_info(numerators=['OIII5007', 'NII6583'],
                             denominators=['Hbeta', 'Halpha'])

    # plot all the spaxels without selecting them first
    # rainbow.plot.bpt(all_spaxels, save=os.path.join(outputdir, 'bpt_all_spaxels.png'))
    # if not np.isnan(cdelt):
    #     rainbow.plot.spaxel_map(all_spaxels, save=os.path.join(outputdir, 'map_all_spaxels.png'))

    'select spaxels'
    # this is a proposal how the spaxels can be classified and selected
    # comment this part if you use an already pre-selected table of spaxels

    # the spaxels which belong to the galaxy
    galaxy_spaxels = all_spaxels.subset(
        np.where(np.all([
            all_spaxels.emission_lines['Halpha'].SNR > 3.,  # the spaxels are useful anly if there is some Halpha signal
            # all_spaxels.radius <= maximum_radius,  # this is useful if there is another object in the FoV you want to clip
            # all_spaxels.radius >= minimum_radius,  # this clips the central circle if necessary
            all_spaxels.emission_lines['Halpha'].error > 0.,  # there are nan in the errors; it filters here, but produces a warning
            all_spaxels.emission_lines['Hbeta'].error > 0.,
            all_spaxels.emission_lines['OIII5007'].error > 0.,
            all_spaxels.emission_lines['NII6583'].error > 0.,
        ], axis=0))[0]
    )
    # all_spaxels.radius can be only used if cdelt is specified

    # the 'excellent' spaxels are the ones which certainly belong to the galaxy
    # and have all the lines fitted properly
    excellent_index = np.where(np.all([
            galaxy_spaxels.emission_lines['Halpha'].SNR > 5.,
            galaxy_spaxels.emission_lines['Hbeta'].SNR > 5.,
            galaxy_spaxels.emission_lines['OIII5007'].SNR > 5.,
            galaxy_spaxels.emission_lines['NII6583'].SNR > 5.,
        ], axis=0))[0]
    excellent_spaxels = galaxy_spaxels.subset(excellent_index)

    other_spaxels = galaxy_spaxels.subset(excellent_index, inverse=True)
    # here I try to filter the spaxels with a failed fit
    # if the velocity differs from the excellent spaxels range, it might be a hint for the fitting problems
    # same if the dispersion is higher
    # this highly depends on the data and objects, so double-check if this makes sense
    other_spaxels = other_spaxels.subset(
        np.where(
            np.all([
                other_spaxels.emission_lines['Halpha'].vel >= excellent_spaxels.emission_lines['Halpha'].vel.min(),
                other_spaxels.emission_lines['Halpha'].vel <= excellent_spaxels.emission_lines['Halpha'].vel.max(),
                other_spaxels.emission_lines['Halpha'].disp <= excellent_spaxels.emission_lines['Halpha'].disp.max(),
            ], axis=0)
        )[0]
    )

    # good spaxels have at least Halpha and Hbeta detection, so the extinction correction can be made
    good_index = np.where(np.all(
        np.append(
            np.array([other_spaxels.emission_lines[line_name].SNR
                      for line_name in other_spaxels.emission_lines.keys()]) > 1.,
            [
                other_spaxels.emission_lines['Halpha'].SNR > 3.,
                other_spaxels.emission_lines['Hbeta'].SNR > 3.,
            ]
            , axis=0)
        , axis=0))[0]
    good_spaxels = other_spaxels.subset(good_index)

    additional_spaxels = other_spaxels.subset(good_index, inverse=True)

    excellent_and_good_spaxels = excellent_spaxels.append(good_spaxels)
    excellent_and_good_spaxels.correct_extinction()

    # even though we selected the quality of the spaxels to be reasonable,
    # there are still ones with negative attenuation a_v
    positive_a_v_index = np.where(excellent_and_good_spaxels.a_v > 0.)[0]
    positive_a_v_spaxels = excellent_and_good_spaxels.subset(positive_a_v_index)
    positive_a_v_spaxels.set_bpt_info(numerators=['OIII5007', 'NII6583'],
                                      denominators=['Hbeta', 'Halpha'])

    # for those I use a median attenuation to correct extinction
    # as well as for the 'additional spaxels'
    median_a_v = np.median(positive_a_v_spaxels.a_v)
    median_a_v_spaxels = excellent_and_good_spaxels.subset(positive_a_v_index,
                                                           inverse=True)
    median_a_v_spaxels.correct_extinction(a_v=median_a_v)
    median_a_v_spaxels.set_bpt_info(numerators=['OIII5007', 'NII6583'],
                                    denominators=['Hbeta', 'Halpha'])

    additional_spaxels.correct_extinction(a_v=median_a_v)

    three_fraction_plot(positive_a_v_spaxels.append(median_a_v_spaxels), image=image, savedir=outputdir)

    'set role'
    # assigning the roles 'agn_basis', 'sf_basis', and 'to_fit' to be used in rainbow.fit

    # do change the default agn_min_bpt_y in the logfile to smth like 0.23 (not physical, but ...)
    # for MRK1044, otherwise there will be no AGN basis spaxels
    agn_basis_index = np.where(np.all([
        np.log10(positive_a_v_spaxels.BPT['OIII5007/Hbeta']) < agn_max_bpt_y,
        np.log10(positive_a_v_spaxels.BPT['OIII5007/Hbeta']) >= agn_min_bpt_y,
        np.log10(positive_a_v_spaxels.BPT['NII6583/Halpha']) > agn_min_bpt_x,
        # positive_a_v_spaxels.radius < dividing_radius,
        # positive_a_v_spaxels.radius < agn_radius,
        positive_a_v_spaxels.emission_lines['Halpha'].SNR > min_agn_basis_snr,
    ], axis=0))[0]
    agn_basis = positive_a_v_spaxels.subset(agn_basis_index)
    agn_basis.sf_fraction = np.zeros(len(agn_basis.emission_lines['Halpha'].flux))
    agn_basis.sf_fraction_error = np.zeros(len(agn_basis.emission_lines['Halpha'].flux))
    try:  # to write the number of agn basis spaxels in the logging file
        logging.info(f'{len(agn_basis.binning)} AGN bases: {len(np.where(agn_basis.binning > 1)[0])} binned; '
                     f'min SNR = {agn_basis.emission_lines["Halpha"].SNR.min()}; '
                     f'mean SNR = {agn_basis.emission_lines["Halpha"].SNR.mean()}')
    except ValueError:
        logging.warning('No AGN bases')

    sf_basis_index = np.where(np.all([
        np.log10(positive_a_v_spaxels.BPT['OIII5007/Hbeta']) < 0.61 / (
                np.log10(positive_a_v_spaxels.BPT['NII6583/Halpha']) - 0.05) + 1.3,  # [Kauffmann 2003] line
        np.log10(positive_a_v_spaxels.BPT['NII6583/Halpha']) < 0.,
        # positive_a_v_spaxels.radius < dividing_radius,
        positive_a_v_spaxels.emission_lines['Halpha'].SNR > min_sf_basis_snr,
    ], axis=0))[0]
    sf_basis = positive_a_v_spaxels.subset(sf_basis_index)
    sf_basis.sf_fraction = np.ones(len(sf_basis.emission_lines['Halpha'].flux))
    sf_basis.sf_fraction_error = np.zeros(len(sf_basis.emission_lines['Halpha'].flux))
    try:  # to write the number of sf basis spaxels in the logging file
        logging.info(f'{len(sf_basis.binning)} SF bases: {len(np.where(sf_basis.binning > 1)[0])} binned; '
                     f'min SNR = {sf_basis.emission_lines["Halpha"].SNR.min()}; '
                     f'mean SNR = {sf_basis.emission_lines["Halpha"].SNR.mean()}')
    except ValueError:
        logging.warning('No SF bases')

    positive_a_v_spaxels_without_basis = positive_a_v_spaxels.subset(np.append(agn_basis_index, sf_basis_index),
                                                                     inverse=True)

    positive_a_v_spaxels_without_basis_and_median_a_v_spaxels = positive_a_v_spaxels_without_basis.append(
        median_a_v_spaxels)

    # this is a pure AGN cloud
    agn_ionized_index = np.where(
        np.all([
            np.log10(positive_a_v_spaxels_without_basis_and_median_a_v_spaxels.BPT['OIII5007/Hbeta']) > 0.61 /
            (np.log10(positive_a_v_spaxels_without_basis_and_median_a_v_spaxels.BPT['NII6583/Halpha']) - 0.47) + 1.19,
            # next 2 lines remove LINER region
            # np.log10(positive_a_v_spaxels_without_basis_and_median_a_v_spaxels.BPT['OIII5007/Hbeta']) > 1.89 *
            # np.log10(positive_a_v_spaxels_without_basis_and_median_a_v_spaxels.BPT['NII6583/Halpha']) + 0.76,
            # positive_a_v_spaxels_without_basis_and_median_a_v_spaxels.radius > agn_radius,
        ], axis=0)
    )[0]
    agn_ionized_spaxels = positive_a_v_spaxels_without_basis_and_median_a_v_spaxels.subset(agn_ionized_index)
    agn_ionized_spaxels.sf_fraction = np.zeros(len(agn_ionized_spaxels.emission_lines['Halpha'].flux))
    agn_ionized_spaxels.sf_fraction_error = np.zeros(len(agn_ionized_spaxels.emission_lines['Halpha'].flux))
    positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud = \
        positive_a_v_spaxels_without_basis_and_median_a_v_spaxels.subset(agn_ionized_index, inverse=True)

    # mixed spaxels are the ones to be fit
    mixed_index = np.where(
        np.any([
            np.all([
                np.any([
                    np.log10(positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.BPT[
                                 'OIII5007/Hbeta']) >= 0.61 /
                    (np.log10(
                        positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.BPT[
                            'NII6583/Halpha']) - 0.05) + 1.3,
                    np.log10(positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.BPT[
                                 'NII6583/Halpha']) >= 0.,
                ], axis=0),
                # positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.radius < dividing_radius,
            ], axis=0),
            np.all([
                np.log10(positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.BPT[
                             'OIII5007/Hbeta']) > 0.61 /
                (np.log10(
                    positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.BPT[
                        'NII6583/Halpha']) - 0.47) + 1.19,
                # positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.radius > dividing_radius,
            ], axis=0)
        ], axis=0)
    )[0]

    mixed_spaxels = positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.subset(mixed_index)

    # pure SF cloud
    sf_ionized_spaxels = positive_a_v_spaxels_without_basis_and_median_a_v_spaxels_without_agn_cloud.subset(mixed_index,
                                                                                                            inverse=True)
    sf_ionized_spaxels.sf_fraction = np.ones(len(sf_ionized_spaxels.emission_lines['Halpha'].flux))
    sf_ionized_spaxels.sf_fraction_error = np.zeros(len(sf_ionized_spaxels.emission_lines['Halpha'].flux))

    'status & role plots'
    plot_status_bpt(agn_basis, sf_basis, mixed_spaxels,
                    sf_ionized_spaxels, agn_ionized_spaxels,
                    save=os.path.join(outputdir, 'status_BPT.png'))
    if not np.isnan(cdelt):
        plot_status_map(agn_basis, sf_basis, mixed_spaxels,
                        sf_ionized_spaxels, agn_ionized_spaxels, additional_spaxels, image,
                        dividing_radius, maximum_radius, minimum_radius, agn_radius,
                        save=os.path.join(outputdir, 'status_map.png'))

    if arguments.status:
        plt.show()
    plt.close('all')
    gc.collect()

    'pickle spaxels'
    # the spaxels with the roles are pickled to be used in `rainbow --logfile ...'
    # to do so there sould be the paths from the datadir specified:
    # agn_basis output/agn_basis.p
    # sf_basis output/sf_basis.p
    # to_fit output/mixed_spaxels.p
    # this is done automaticaly below, if was not specified before (then it won't be overwritten)
    with open(outputdir + 'agn_basis.p', 'wb') as f:
        pickle.dump(agn_basis, f)
    with open(outputdir + 'sf_basis.p', 'wb') as f:
        pickle.dump(sf_basis, f)
    with open(outputdir + 'mixed_spaxels.p', 'wb') as f:
        pickle.dump(mixed_spaxels, f)
    with open(outputdir + 'sf_ionized_spaxels.p', 'wb') as f:
        pickle.dump(sf_ionized_spaxels, f)
    with open(outputdir + 'agn_ionized_spaxels.p', 'wb') as f:
        pickle.dump(agn_ionized_spaxels, f)
    with open(outputdir + 'additional_spaxels.p', 'wb') as f:
        pickle.dump(additional_spaxels, f)

    'check logfile rainbow input'
    write_agn_basis = True
    write_sf_basis = True
    write_to_fit = True
    with open(arguments.logfile, 'r') as logfile:
        lines = logfile.readlines()
        for line in lines:
            if 'agn_basis ' in line:
                print(line, end='')
                write_agn_basis = False
            if 'sf_basis ' in line:
                print(line, end='')
                write_sf_basis = False
            if 'to_fit ' in line:
                print(line, end='')
                write_to_fit = False

    if write_agn_basis:
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'agn_basis {os.path.relpath(os.path.join(outputdir, "agn_basis.p"), datadir)}\n')
            logging.info(f'agn_basis {os.path.relpath(os.path.join(outputdir, "agn_basis.p"), datadir)}')
    if write_sf_basis:
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'sf_basis {os.path.relpath(os.path.join(outputdir, "sf_basis.p"), datadir)}\n')
            logging.info(f'sf_basis {os.path.relpath(os.path.join(outputdir, "sf_basis.p"), datadir)}')
    if write_to_fit:
        with open(arguments.logfile, 'a') as logfile:
            logfile.write(f'to_fit {os.path.relpath(os.path.join(outputdir, "mixed_spaxels.p"), datadir)}\n')
            logging.info(f'to_fit {os.path.relpath(os.path.join(outputdir, "mixed_spaxels.p"), datadir)}')

    'writing output table'
    # the spaxels can be also stored as tables if needed
    # example:
    # rainbow.write.fits_table(agn_basis, os.path.join(outputdir + 'agn_basis.fits'))

    logging.info('Finished {:.2f} min'.format((time.time() - start_time) / 60.))
    # when this code is finished run
    # `rainbow --logfile ./logfile.txt -sr`
    # to fit the result
    # (do not forget to make sure you have your AGN basis spectra selected for MRK1044
    #  for that you might need to set an unphysical threshhold of agn_min_bpt_y ~ 0.23)
    # 'fitted_spaxels.p' will appear in the outputdir
    # use `fitted_spaxels = pickle.load(open(os.path.join(outputdir + 'fitted_spaxels.p'), 'rb'))`
    # to read the pickled object