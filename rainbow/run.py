import pickle
import time
import os
import rainbow.read
import rainbow.plot
import rainbow.fit
import matplotlib.pyplot as plt
import gc


def run(arguments):
    """

    Args:
        arguments:
            --logfile <path to the file with all other arguments>
        in the logfile:
            the structure should look like:
            '<argument_name> <argument_value>'
            with one argument per line
            ## Mandatory arguments:
            datadir: data directory path
            agn_basis: filename in datadir of the pickeled object of Spaxel or EmissionLineSet class;
                       will be used as AGN basis in the fitting
            sf_basis: same as agn_basis;
                      will be used as SF basis in the fitting
            to_fit: same as agn_basis;
                    will be fitted
            ## Additional arguments:
            outputdir: output directory path; same as datadir if not specified
            galname: galaxy name
            lines: emission line names as listed in the fits table;
                   should be separated by comma without spaces;
                   for example: Halpha,Hbeta,OIII5007,NII6583
    Output:
        pickeled object with the fitted spaxels or emission line set;
        'rainbow_result_BPT.png' in the output directory

    """
    start_time = time.time()

    'define datadir & outputdir'
    try:
        datadir = arguments.datadir
        print('Data directory: {}'.format(datadir))
    except AttributeError:
        raise AttributeError('datadir argument is mandatory')
    try:
        outputdir = arguments.outputdir
    except AttributeError:
        outputdir = datadir
    try:
        os.makedirs(outputdir)
        print('Output directory ', outputdir, 'created')
    except FileExistsError:
        print('Output directory: {}'.format(outputdir))

    'read data'
    read_time = time.time()

    agn_basis = pickle.load(open(os.path.join(datadir + arguments.agn_basis), 'rb'))
    sf_basis = pickle.load(open(os.path.join(datadir + arguments.sf_basis), 'rb'))
    to_fit = pickle.load(open(os.path.join(datadir + arguments.to_fit), 'rb'))

    print('Data read {:.4f} min'.format((time.time() - read_time) / 60.))

    'plot status'
    rainbow.plot.bpt(to_fit, color='green', show=0, label='Mixed')
    rainbow.plot.bpt(agn_basis, color='red', show=0, size=0, label='AGN Basis')
    rainbow.plot.bpt(sf_basis, color='blueviolet', show=0, size=0, label='SF Basis', legend=0,
                     save=os.path.join(outputdir, 'rainbow_status_BPT.png'))
    if arguments.status:
        plt.show()
        plt.close('all')
        gc.collect()
    else:
        plt.close('all')
        gc.collect()

    'fit'
    print('Starting MCMC fitting:')
    try:
        proc_number = int(arguments.proc_number)
    except AttributeError:
        proc_number = None
    fitted = rainbow.fit(to_fit, agn_basis, sf_basis, proc_number=proc_number)
    with open(outputdir + 'fitted_spaxels.p', 'wb') as f:
        pickle.dump(fitted, f)

    'plot result'
    cmap = plt.get_cmap('rainbow_r')
    rainbow.plot.bpt(agn_basis, color='red', show=0)
    rainbow.plot.bpt(sf_basis, color='blueviolet', size=0, show=0)
    rainbow.plot.bpt(fitted, colorcode=fitted.sf_fraction,
                     cmap=cmap, clim=(0., 1.),
                     clb=1, clb_label='SF fraction',
                     vmin=0., vmax=1.,
                     size=0, show=0,
                     legend=0,
                     save=os.path.join(outputdir, 'rainbow_result_BPT.png')
                     )
    if arguments.result:
        plt.show()
        plt.close('all')
        gc.collect()
    else:
        plt.close('all')
        gc.collect()

    print('Finished {:.2f} min'.format((time.time() - start_time) / 60.))
