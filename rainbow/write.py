import astropy.io.fits as pf
# ToDo: add fits headers


def fits_image(image_data, image_error=None, outputfile='file.fits'):
    """
    Saves an image fits file with an optional error extension.
    Args:
        image_data: numpy array or other arrays, acceptable by astropy.io.fits
        image_error:
        outputfile:
    """
    if image_error is not None:
        with pf.HDUList([pf.PrimaryHDU(image_data), pf.ImageHDU(image_error, name='ERROR')]) as out:
            out.writeto(outputfile, overwrite=True)
    else:
        with pf.HDUList(pf.PrimaryHDU(image_data)) as out:
            out.writeto(outputfile, overwrite=True)


def fits_table(output_object, outputfile='table.fits'):
    """
    Saves a fits table.
    Args:
        output_object: Spaxel class instance
        outputfile:

    """
    cols = list([])
    cols.append(pf.Column(name='x', format='D', array=output_object.x))
    cols.append(pf.Column(name='y', format='D', array=output_object.y))
    for emission_line_name in list(output_object.emission_lines.keys()):
        cols.append(pf.Column(name=emission_line_name + '_flux', format='D',
                                  array=output_object.emission_lines[emission_line_name].flux))
        if not output_object.emission_lines[emission_line_name].error_empty:
            cols.append(pf.Column(name=emission_line_name + '_flux_err', format='D',
                                  array=output_object.emission_lines[emission_line_name].error))
        if not output_object.emission_lines[emission_line_name].vel_empty:
            cols.append(pf.Column(name=emission_line_name + '_vel', format='D',
                                  array=output_object.emission_lines[emission_line_name].vel))
        if not output_object.emission_lines[emission_line_name].vel_err_empty:
            cols.append(pf.Column(name=emission_line_name + '_vel_err', format='D',
                                  array=output_object.emission_lines[emission_line_name].vel_err))
        if not output_object.emission_lines[emission_line_name].fwhm_empty:
            cols.append(pf.Column(name=emission_line_name + '_fwhm', format='D',
                                  array=output_object.emission_lines[emission_line_name].fwhm))
        if not output_object.emission_lines[emission_line_name].fwhm_err_empty:
            cols.append(pf.Column(name=emission_line_name + '_fwhm_err', format='D',
                                  array=output_object.emission_lines[emission_line_name].fwhm_err))
        if not output_object.emission_lines[emission_line_name].disp_empty:
            cols.append(pf.Column(name=emission_line_name + '_disp', format='D',
                                  array=output_object.emission_lines[emission_line_name].disp))
        if not output_object.emission_lines[emission_line_name].disp_err_empty:
            cols.append(pf.Column(name=emission_line_name + '_disp_err', format='D',
                                  array=output_object.emission_lines[emission_line_name].disp_err))
    cols.append(pf.Column(name='SF_fraction', format='D', array=output_object.sf_fraction))
    cols.append(pf.Column(name='SF_fraction_error', format='D', array=output_object.sf_fraction_error))
    hdu = pf.BinTableHDU.from_columns(cols)
    print('Writing FITS table {}'.format(outputfile))
    hdu.writeto(outputfile, overwrite=True)
