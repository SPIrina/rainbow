import argparse


def read_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--logfile')
    parser.add_argument('--example', action='store_true')
    parser.add_argument('-s', '--status', action='store_true')
    parser.add_argument('-d', '--demarcation', action='store_true')
    parser.add_argument('-r', '--result', action='store_true')
    parser.add_argument('-p', '--pickle', action='store_true')
    args = parser.parse_args()
    logfile = args.logfile
    if logfile is not None:
        read_parser_file(logfile, args)
    return args


def read_parser_file(file, args):
    with open(file) as f:
        lines = f.readlines()
    for line in lines:
        try:
            arg, value = line.split()
            setattr(args, arg, value)
        except IOError:
            print('Should be 2 words in a line: arg, value \nGot: {}'.format(line))
