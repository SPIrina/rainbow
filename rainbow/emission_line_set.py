import numpy as np
from rainbow import EmissionLine


class EmissionLineSet:
    def __init__(self):
        """
        Emission Line Set class:
        contains dictionary of EmissionLine objects, called by their names;
        with a set of emission lines the BPT ratios can be defined in the corresponding dictionary
        by the 'set_bpt_info' function;
        the extinction correction can be applied with 'correct_extinction' if Halpha and Hbeta lines are presented,
        or if a_v is additionaly specified,
        then the atthenuation a_v will be stored;
        the SF fraction and error can be stored as well with 'add_fitting_result' function.
        """
        self.emission_lines = {}
        self.BPT = {}
        self.a_v = np.array([np.nan])
        self.sf_fraction = np.array([np.nan])
        self.sf_fraction_error = np.array([np.nan])

    def add_line(self, emission_line):
        """

        Args:
            emission_line: an object of EmissionLine class

        """
        self.emission_lines[emission_line.name] = emission_line
        self.check_shapes(emission_line.name)

    def check_shapes(self, emission_line_name):
        shape = self.emission_lines[emission_line_name].flux.shape
        self.a_v = EmissionLine.check_shape(self.a_v, shape)
        self.sf_fraction = EmissionLine.check_shape(self.sf_fraction, shape)
        self.sf_fraction_error = EmissionLine.check_shape(self.sf_fraction_error, shape)

    def correct_extinction(self, a_v=np.nan):
        if np.isnan(a_v):
            a_v = self.calculate_a_v()
        self.a_v = np.ones(len(self.emission_lines['Halpha'].flux)) * a_v
        for s in self.emission_lines.keys():
            self.emission_lines[s] = self.extinction_correction(self.emission_lines[s], a_v)

    @staticmethod
    def extinction_correction(line, a_v, r_v=3.1):
        """

        Args:
            line: object EmissionLine
            a_v: attenuation [mag]
            r_v: 3.1 [Cardelli, Clayton, & Mathis 1989] http://adsabs.harvard.edu/abs/1989ApJ...345..245C

        Returns: object with corrected flux & error

        """
        x = 1. / (line.wavelength * 1e-4)  # [um-1]
        y = x - 1.82
        a = 1. + 0.17699 * y - 0.50447 * y ** 2. - 0.02427 * y ** 3. + 0.72085 * y ** 4. + 0.01979 * y ** 5. - 0.77530 * y ** 6. + 0.32999 * y ** 7.
        b = 1.41338 * y + 2.28305 * y ** 2. + 1.07233 * y ** 3. - 5.38434 * y ** 4. - 0.62251 * y ** 5. + 5.30260 * y ** 6. - 2.09002 * y ** 7.
        a_lambda_over_a_v = a + b / r_v
        a_lambda = a_lambda_over_a_v * a_v
        line.flux = line.flux * 10. ** (0.4 * a_lambda)
        # no need in taking into account BD error - the fluxes are coupled
        line.error = line.error * 10. ** (0.4 * a_lambda)
        return line

    def calculate_a_v(self):
        """

        Returns: attenuation [mag]

        """
        a_v = np.ones(len(self.emission_lines['Halpha'].flux)) * np.nan
        ind = np.where(np.all([self.emission_lines['Halpha'].flux >= 0.,
                               self.emission_lines['Hbeta'].flux > 0.], axis=0))[0]
        balmer_decrement = self.emission_lines['Halpha'].flux[ind] / self.emission_lines['Hbeta'].flux[ind]
        a_halpha_over_a_v = 0.8177790395288114  # calculates from the above formulae
        a_hbeta_over_a_v = 1.1641718080823185
        a_v[ind] = -(2.5 * np.log10(balmer_decrement / 2.86)) / (a_halpha_over_a_v - a_hbeta_over_a_v)
        return a_v

    def set_bpt_info(self,
                     numerators=['OIII5007', 'NII6583'],
                     denominators=['Hbeta', 'Halpha']):
        for numerator, denominator in zip(numerators, denominators):
            if not (numerator in self.emission_lines.keys() and
                    denominator in self.emission_lines.keys()):
                raise KeyError('Listed emission line names are not found: {}'.format(self.emission_lines.keys()))

            ratio = np.empty(self.emission_lines[denominator].flux.shape) * np.nan
            ratio_err = np.empty(ratio.shape) * np.nan
            ind = np.where(np.all([self.emission_lines[numerator].flux >= 0.,
                                   self.emission_lines[denominator].flux > 0.], axis=0))[0]
            ratio[ind] = self.emission_lines[numerator].flux[ind] / self.emission_lines[denominator].flux[ind]
            try:
                ind = np.where(np.all([self.emission_lines[numerator].flux >= 0.,
                                       self.emission_lines[denominator].flux > 0.,
                                       self.emission_lines[numerator].error >= 0.,
                                       self.emission_lines[denominator].error >= 0.], axis=0))[0]
                ratio_err[ind] = np.sqrt((self.emission_lines[numerator].error[ind] /
                                          self.emission_lines[denominator].flux[ind]) ** 2. +
                                         (self.emission_lines[numerator].flux[ind] *
                                          self.emission_lines[denominator].error[ind] /
                                          self.emission_lines[denominator].flux[ind] ** 2.) ** 2.)
            except ValueError:
                pass

            self.BPT[numerator + '/' + denominator] = ratio
            self.BPT[numerator + '/' + denominator + '_err'] = ratio_err

    def add_fitting_result(self, sf, sf_error):
        self.sf_fraction = sf
        self.sf_fraction_error = sf_error

    def subset(self, ind, inverse=False):
        subset = EmissionLineSet()

        if inverse:
            mask = np.ones(len(self.emission_lines[list(self.emission_lines.keys())[0]].flux), np.bool)
            mask[ind] = 0
            ind = mask

        for line_name in list(self.emission_lines.keys()):
            subset.add_line(self.emission_lines[line_name].subset(ind, inverse=False))

        for key in self.BPT.keys():
            subset.BPT[key] = self.BPT[key][ind]

        subset.a_v = self.a_v[ind]
        subset.sf_fraction = self.sf_fraction[ind]
        subset.sf_fraction_error = self.sf_fraction_error[ind]
        return subset

    def append(self, emission_line_set):
        result = EmissionLineSet()
        for line_name in list(self.emission_lines.keys()):
            result.add_line(self.emission_lines[line_name].append(emission_line_set.emission_lines[line_name]))
        for key in self.BPT.keys():
            result.BPT[key] = np.append(self.BPT[key], emission_line_set.BPT[key])
        result.a_v = np.append(self.a_v, emission_line_set.a_v)
        result.sf_fraction = np.append(self.sf_fraction, emission_line_set.sf_fraction)
        result.sf_fraction_error = np.append(self.sf_fraction_error, emission_line_set.sf_fraction_error)
        return result
