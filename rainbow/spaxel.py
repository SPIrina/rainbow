import numpy as np
from rainbow import EmissionLineSet
import matplotlib.pyplot as plt


class Spaxel(EmissionLineSet):
    def __init__(
            self,
            x=np.array([np.nan]),
            y=np.array([np.nan]),
            binning=1
    ):
        """
        Spaxel class for IFU data;
        contains spatial information;
        allows to plot a spatial map with rainbow.plot.map.
        The Spaxel class inherits EmissionLineSet class properties.

        Args:
            x: position on X-axis in pixels, start with 0, integer
            y: position on Y-axis in pixels, start with 0, integer
            binning: 1 for not binned spaxels, 2 for 2x2 binned spaxels, etc.;
                     specifying binning is necessery to overplot with rainbow.plot.map
                     if different binning is presented.

        """
        EmissionLineSet.__init__(self)
        self.x = x
        self.y = y
        self.binning = np.zeros(self.x.shape, dtype=int) + binning
        self.cdelt = np.nan
        self.nx = np.nan
        self.ny = np.nan
        self.x_cen = np.nan
        self.y_cen = np.nan
        self.radius = np.array([np.nan])

    def add_emission_line_set_info(self, emission_line_set):
        """
        Adds all the information from the EmissionLineSet class
        """
        for emission_line_name in list(emission_line_set.emission_lines.keys()):
            self.add_line(emission_line_set.emission_lines[emission_line_name])
        self.BPT = emission_line_set.BPT
        self.a_v = emission_line_set.a_v
        self.sf_fraction = emission_line_set.sf_fraction
        self.sf_fraction_error = emission_line_set.sf_fraction_error

    def set_spatial_info(self, nx, ny, cdelt, x_cen=np.nan, y_cen=np.nan):
        """
        Set spatial info to be able to use rainbow.plot.map.

        Args:
            nx: number of pixels along X-axis
            ny: number of pixels along Y-axis
            cdelt: arcsec in one pixel
            x_cen: central pixel along X-axis, start with 0
            y_cen: central pixel along Y-axis, start with 0

        Returns:
            Updates the listed arguments, including
            radius: distance to the center in arcsec

        """
        self.nx = nx
        self.ny = ny
        self.cdelt = cdelt
        self.x_cen = x_cen
        self.y_cen = y_cen
        self.radius = np.sqrt((self.x - ((self.x_cen + 0.5) / self.binning - 0.5)) ** 2. +
                              (self.y - ((self.y_cen + 0.5) / self.binning - 0.5)) ** 2.) * self.cdelt * self.binning

    def remove_shealded(self, front_spaxels=None, save=None):
        """
        Args:
            front_spaxels: spaxels which shield the spatial map; if None the subset with binning is 1 play this role
            save: path for saving visibility map with 1 stored for back spaxels and 0 for front spaxels

        Returns:
            Spaxel object with shealded spaxels removed
        """
        visibility_map = np.full([self.ny, self.nx], np.nan)

        if front_spaxels is None:
            front_index = np.where(self.binning == 1)[0]
            front_spaxels = self.subset(front_index)
            back_spaxels = self.subset(front_index, inverse=True)
            return_front = True
        else:
            back_spaxels = self
            return_front = False
        for ind in range(len(back_spaxels.x)):
            x_mesh, y_mesh = np.meshgrid(back_spaxels.x[ind] * back_spaxels.binning[ind] +
                                         np.arange(back_spaxels.binning[ind]),
                                         back_spaxels.y[ind] * back_spaxels.binning[ind] +
                                         np.arange(back_spaxels.binning[ind]))
            visibility_map[y_mesh, x_mesh] = 1
        for ind in range(len(front_spaxels.x)):
            x_mesh, y_mesh = np.meshgrid(front_spaxels.x[ind] * front_spaxels.binning[ind] +
                                         np.arange(front_spaxels.binning[ind]),
                                         front_spaxels.y[ind] * front_spaxels.binning[ind] +
                                         np.arange(front_spaxels.binning[ind]))
            visibility_map[y_mesh, x_mesh] = 0
        if save is not None:
            fig = plt.figure()
            plt.imshow(visibility_map, origin='lower')
            plt.savefig(save)
            plt.close(fig)
        visible_ind = []
        for ind in range(len(back_spaxels.x)):
            x_mesh, y_mesh = np.meshgrid(back_spaxels.x[ind] * back_spaxels.binning[ind] +
                                         np.arange(back_spaxels.binning[ind]),
                                         back_spaxels.y[ind] * back_spaxels.binning[ind] +
                                         np.arange(back_spaxels.binning[ind]))
            if np.nansum(visibility_map[y_mesh, x_mesh]) >= 1:
                visible_ind = visible_ind + [ind]
        if return_front:
            return front_spaxels.append(back_spaxels.subset(visible_ind))
        else:
            return back_spaxels.subset(visible_ind)

    def subset(self, ind, inverse=False):
        subset = Spaxel()

        if inverse:
            mask = np.ones(len(self.emission_lines[list(self.emission_lines.keys())[0]].flux), np.bool)
            mask[ind] = 0
            ind = mask

        subset.add_emission_line_set_info(super(Spaxel, self).subset(ind, inverse=False))

        subset.x = self.x[ind]
        subset.y = self.y[ind]
        subset.binning = self.binning[ind]
        subset.cdelt = self.cdelt
        subset.nx = self.nx
        subset.ny = self.ny
        subset.x_cen = self.x_cen
        subset.y_cen = self.y_cen
        subset.radius = self.radius[ind]
        return subset

    def append(self, spaxel):
        result = Spaxel()

        result.add_emission_line_set_info(super(Spaxel, self).append(spaxel))

        result.x = np.append(self.x, spaxel.x)
        result.y = np.append(self.y, spaxel.y)
        result.binning = np.append(self.binning, spaxel.binning)
        result.cdelt = self.cdelt
        result.nx = self.nx
        result.ny = self.ny
        result.x_cen = self.x_cen
        result.y_cen = self.y_cen
        result.radius = np.append(self.radius, spaxel.radius)
        return result
