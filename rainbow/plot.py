import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import gc


def bpt(emission_line_set,
        show=1, size=1,
        figsize=(3., 3.), dpi=300,
        dividing_lines=0, x_label=1, y_label=1,
        xlim=(-1., 0.25), ylim=(-1., 1.25),
        save=None, transparent=False,
        legend=0,
        colorcode=None, color='0.4',
        s=9, s_min=4., s_max=400.,
        typical_error=0, typical_error_x=None, typical_error_y=None,
        clb=0, clb_label='', clb_ticks=None,
        sii=False,
        **kwargs):
    """

    Args:
        emission_line_set: EmissionLineSet (or Spaxel) class instance
        show: if 1 calls plt.show() in the end and closes the figure
        size: if 1 creates a matplotlib figure with the given figsize and dpi;
              if 1 also plots the demarcation lines on the plot
        figsize:
        dpi:

        dividing_lines: if 1 plots the demarcation lines on the existing figure

        x_label: if 1 puts an x label;
                 if 0 does not show x tick labels
        y_label: same for y
        xlim: x axis limit
        ylim:

        save: put here a filename string and the plot will be saved
        transparent: bool; makes the saved file backfround transparent

        legend: if 1 calls plt.legend()

        typical_error: if 1 puts a grey dot with errorbars, calculated as median error from the data
        typical_error_x: the position of the typical error symbol can be either specified here,
                         or left default (lower left corner)
        typical_error_y:

        clb: if 1 calls plt.colorbar
        clb_label:
        clb_ticks:

        sii: bool; makes [SII] BPT instead of [NII]

        arguments used in plt.scatter:
        colorcode:
        color:
        s: size of the dots used in scatter;
           if a number all the dots are same size;
           if an array the dot sizes will be ranged from s_min to s_max:
           this allows to visualize, for exapmle, the brightest points with larger circles
        **kwargs: additional argumrnts for plt.scatter

    """
    if size == 1:
        plt.figure(figsize=figsize, dpi=dpi)
        'adding dividing lines'
    if size == 1 or dividing_lines == 1:
        if sii:
            xdashed = np.arange(-1.5, 0.25, 1.5 / 100.)
            ydashed = 0.72 / (xdashed - 0.32) + 1.3
            plt.plot(xdashed, ydashed, '--k', label='[Kewley+2006]')  # extreme straburst
            xdotted = np.arange(-0.31, 1., 1.5 / 100.)
            ydotted = 1.89 * xdotted + 0.76
            plt.plot(xdotted, ydotted, 'k', linestyle='dotted', label='[Kewley+2006]')  # Seyfert-LINER
            plt.xlim(xlim[0], xlim[1])
        else:
            xsolid = np.arange(-1.5, 0., 1.5 / 100.)
            ysolid = 0.61 / (xsolid - 0.05) + 1.3
            plt.plot(xsolid, ysolid, '-k', label='[Kauffmann+2003]')  # pure star formation
            xdashed = np.arange(-1.5, 0.25, 1.5 / 100.)
            ydashed = 0.61 / (xdashed - 0.47) + 1.19
            plt.plot(xdashed, ydashed, '--k', label='[Kewley+2001]')  # extreme straburst
            xdotted = np.arange(-0.2, 1., 1.5 / 100.)
            ydotted = 1.01 * xdotted + 0.48
            plt.plot(xdotted, ydotted, 'k', linestyle='dotted', label='[Cid Fernandes+2010]')  # Seyfert-LINER
            plt.xlim(xlim[0], xlim[1])
        plt.ylim(ylim[0], ylim[1])
        plt.tick_params(axis='both',
                        which='both',  # both major and minor ticks are affected
                        left='on',
                        right='on',
                        top='on',
                        bottom='on',
                        direction='in')

    ind = np.argsort(s)[::-1]
    if len(ind) > 1:
        emission_line_set = emission_line_set.subset(ind)
        s = (s - s.min()) * ((s_max - s_min)/(s.max() - s.min())) + s_min
        s = s[ind]
        if colorcode is not None:
            colorcode = colorcode[ind]

    if sii:
        bpt_x = emission_line_set.BPT['SII6717/Halpha']
        if x_label == 1:
            pltplt.show().xlabel('log([SII]6717/H$\\alpha$)')
        elif x_label == 0:
            ax = plt.gca()
            plt.setp(ax.get_xticklabels(), visible=False)

    else:
        bpt_x = emission_line_set.BPT['NII6583/Halpha']
        if x_label == 1:
            plt.xlabel('log([NII]6583/H$\\alpha$)')
        elif x_label == 0:
            ax = plt.gca()
            plt.setp(ax.get_xticklabels(), visible=False)
    if y_label == 1:
        plt.ylabel('log([OIII]5007/H$\\beta$)')
    else:
        if y_label == 0:
            ax = plt.gca()
            plt.setp(ax.get_yticklabels(), visible=False)
    bpt_y = emission_line_set.BPT['OIII5007/Hbeta']

    if colorcode is not None:
        cax = plt.scatter(np.log10(bpt_x), np.log10(bpt_y), s=s, c=colorcode, **kwargs)
    else:
        cax = plt.scatter(np.log10(bpt_x), np.log10(bpt_y), s=s, color=color, **kwargs)
    if typical_error == 1:
        if typical_error_x is None:
            typical_error_x = xlim[0]+0.15
        if typical_error_y is None:
            typical_error_y = ylim[0]+0.25
        if sii:
            bpt_x_err = emission_line_set.BPT['SII6717/Halpha_err']
        else:
            bpt_x_err = emission_line_set.BPT['NII6583/Halpha_err']
        bpt_y_err = emission_line_set.BPT['OIII5007/Hbeta_err']
        xerr = [np.log10(bpt_x / (bpt_x - bpt_x_err)), np.log10((bpt_x + bpt_x_err) / bpt_x)]
        yerr = [np.log10(bpt_y / (bpt_y - bpt_y_err)), np.log10((bpt_y + bpt_y_err) / bpt_y)]
        plt.errorbar(typical_error_x, typical_error_y, xerr=[[np.nanmedian(xerr[0])], [np.nanmedian(xerr[1])]],
                     yerr=[[np.nanmedian(yerr[0])], [np.nanmedian(yerr[1])]], fmt='.', color='0.5', elinewidth=1.)
    if legend == 1:
        plt.legend()
    if clb == 1:
        plt.colorbar(cax, label=clb_label, ticks=clb_ticks)
    if save is not None:
        plt.savefig(save, transparent=transparent)
    if show == 1:
        plt.show()
        plt.close('all')
        gc.collect()


def spaxel_map(spaxels,
               imshow_map=None,
               show=1, size=1,
               figsize=(3., 3.), dpi=300,
               save=None, transparent=False,
               divide_by_pixsize=1,
               image=None, image_levels=None,
               extent=None,
               clb=0, clb_label='', clb_ticks=None,
               colorcode=None, cmap='viridis', color='0.4',
               **kwargs):
    """
    This function does not only plot the map,
    it also returns a numpy array 'imshow_map' generated from the spaxels.
    This array can be used then, for example, to save in a fits file with rainbow.write.fits_image.

    If the input contains overlapping spaxels, it puts the ones with larger binning to the background.
    Args:
        spaxels: Spaxel class instance
        imshow_map: if calculated before, it can be provided here to avoid unnecesary calculations;
                    spaxels object is then used only to check the size and the center of the map
        show: if 1 calls plt.show() in the end and closes the figure
        size: if 1 creates a matplotlib figure with the given figsize and dpi;
              if 1 also plots the demarcation lines on the plot
        figsize:
        dpi:

        save: put here a filename string and the plot will be saved
        transparent: bool; makes the saved file backfround transparent

        divide_by_pixsize:

        image: an image as an np.array to be overplotted as grey contours
        image_levels: can be specified or left as None for default values

        extent: can be specified as in plt.imshow

        clb: if 1 calls plt.colorbar
        clb_label:
        clb_ticks:

        colorcode:
        cmap:

        color:

        **kwargs: additional arguments to be used in plt.imshow

    Returns: the numpy array 'imshow_map'

    """
    if size == 1:
        plt.figure(figsize=figsize, dpi=dpi)
        plt.xlabel(r'$\Delta$ RA [arcsec]')
        plt.ylabel(r'$\Delta$ DEC [arcsec]')
        plt.tick_params(axis='both',
                        which='both',
                        left='on',
                        right='on',
                        top='on',
                        bottom='on',
                        direction='in')

    nx = spaxels.nx
    ny = spaxels.ny
    xc = spaxels.x_cen
    yc = spaxels.y_cen
    cdelt = spaxels.cdelt
    if np.isnan(xc):
        xc = nx / 2.
    if np.isnan(yc):
        yc = ny / 2.
    x = -cdelt * (np.arange(0, nx) - xc)
    y = cdelt * (np.arange(0, ny) - yc)
    if extent is None:
        extent = [x.max(), x.min(), y.min(), y.max()]
        x_ind = np.arange(0, len(x))
        y_ind = np.arange(0, len(y))
        if image is not None:
            x_mesh_image, y_mesh_image = np.meshgrid(x, y)
    else:
        x_ind = np.where(np.all([
            x <= extent[0],
            x >= extent[1]
        ], axis=0))[0]
        y_ind = np.where(np.all([
            y <= extent[3],
            y >= extent[2]
        ], axis=0))[0]
        if image is not None:
            x_mesh_image, y_mesh_image = np.meshgrid(x[x_ind], y[y_ind])
            image = image[y_ind][:, x_ind]

    if imshow_map is None:
        if colorcode is None:
            colorcode = np.ones(len(spaxels.x))
            cmap = mpl.colors.ListedColormap([color])
        else:
            if len(colorcode) != len(spaxels.x):
                raise IndexError('colorcode does not match with spaxels')

        imshow_map = np.zeros([ny, nx]) * np.nan
        binning = spaxels.binning
        index = np.argsort(binning)[::-1]
        for ind in index:
            x_mesh, y_mesh = np.meshgrid(spaxels.x[ind] * spaxels.binning[ind] +
                                         np.arange(spaxels.binning[ind]),
                                         spaxels.y[ind] * spaxels.binning[ind] +
                                         np.arange(spaxels.binning[ind]))
            if divide_by_pixsize == 0:
                imshow_map[y_mesh, x_mesh] = colorcode[ind]
            else:
                imshow_map[y_mesh, x_mesh] = colorcode[ind] / spaxels.binning[ind] ** 2.
        imshow_map = imshow_map[y_ind][:,x_ind]

    cax = plt.imshow(imshow_map, origin='lower', extent=extent, cmap=cmap, **kwargs)

    if image is not None:
        # assuming image matches with spaxels spatial info
        if image_levels is None:
            image_mean = np.nanmean(np.log10(image))
            image_max = np.nanmax(np.log10(image))
            image_levels = [
                            image_mean + (image_max - image_mean)*0.1,
                            image_mean + (image_max - image_mean)*0.3,
                            image_mean + (image_max - image_mean)*0.5,
                            image_mean + (image_max - image_mean)*0.8
                            ]
        plt.contour(x_mesh_image, y_mesh_image, np.log10(image),
                    levels=image_levels,
                    colors='0.2', alpha=0.6)

    if clb == 1:
        plt.colorbar(cax, label=clb_label, ticks=clb_ticks)
    if save is not None:
        plt.savefig(save, transparent=transparent)
    if show == 1:
        plt.show()
        plt.close('all')
        gc.collect()

    return imshow_map
