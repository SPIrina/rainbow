"""
The NAME_DICTIONARY unifies the emission line name variations.
For example, 'SII6717', 'SII', and 'S2_1' will all be replaced by 'SII6717'.
To avoid this behaviour delete the elements from the dictionary.
To add more name variations add elements to the dictionary.

The WAVELENGHT_DICTIONARY contains the default wavelengths of the lines
and used if a wavelength is not specified.
Change/add elements to the dictionary if necessary.
"""
import numpy as np

NAME_DICTIONARY = {
    'Halpha': 'Halpha',
    'Ha': 'Halpha',
    'Hbeta': 'Hbeta',
    'Hb': 'Hbeta',
    'Hgamma': 'Hgamma',
    'OIII5007': 'OIII5007',
    'OIII': 'OIII5007',
    'O3': 'OIII5007',
    'NII6583': 'NII6583',
    'NII': 'NII6583',
    'N2': 'NII6583',
    'SII6717': 'SII6717',
    'SII': 'SII6717',
    'S2_1': 'SII6717',
    'OII3726': 'OII3726',
    'OII3729': 'OII3729',
    'NeIII3869': 'NeIII3869',
}
WAVELENGHT_DICTIONARY = {
    'Halpha': 6562.79,  # NIST
    'Hbeta': 4861.3,
    'OIII5007': 5006.85,
    'NII6583': 6583.47,
    'SII6717': 6716.44,
    'OII3726': 3726.,
    'OII3729': 3729.,
    'NeIII3869': 3869.,
}


class EmissionLine:
    def __init__(
            self,
            name=None,
            wavelength=np.nan,
            flux=np.array([np.nan]),
            error=np.array([np.nan]),
            vel=np.array([np.nan]),
            vel_err=np.array([np.nan]),
            fwhm=np.array([np.nan]),
            fwhm_err=np.array([np.nan]),
            disp=np.array([np.nan]),
            disp_err=np.array([np.nan])
    ):
        """
        Emission Line class;
        contains information on one line.

        Args:
            name: emission line name
            wavelength: wavelength in Angstrom
            flux: emission line flux
            error: emission line flux error
            vel: velocity
            vel_err: velocity error
            fwhm: FWHM
            fwhm_err: FWHM error
            disp: dispersion
            disp_err: dispersion error
        """
        if name in NAME_DICTIONARY:
            self.name = NAME_DICTIONARY[name]
        else:
            self.name = name
        if not np.isnan(wavelength):
            self.wavelength = wavelength
        else:
            try:
                self.wavelength = WAVELENGHT_DICTIONARY[self.name]
            except KeyError:
                pass
                # raise KeyError('Specify wavelength or name from the {}'.format(name_dictionary.keys()))
        self.flux = flux
        self.error = error
        self.vel = vel
        self.vel_err = vel_err
        self.fwhm = fwhm
        self.fwhm_err = fwhm_err
        self.disp = disp
        self.disp_err = disp_err

        if len(self.flux) == 1 and np.isnan(self.flux[0]):
            self.flux_empty = True
        else:
            self.flux_empty = False
        if len(self.error) == 1 and np.isnan(self.error[0]):
            self.error_empty = True
        else:
            self.error_empty = False
        if len(self.vel) == 1 and np.isnan(self.vel[0]):
            self.vel_empty = True
        else:
            self.vel_empty = False
        if len(self.vel_err) == 1 and np.isnan(self.vel_err[0]):
            self.vel_err_empty = True
        else:
            self.vel_err_empty = False
        if len(self.fwhm) == 1 and np.isnan(self.fwhm[0]):
            self.fwhm_empty = True
        else:
            self.fwhm_empty = False
        if len(self.fwhm_err) == 1 and np.isnan(self.fwhm_err[0]):
            self.fwhm_err_empty = True
        else:
            self.fwhm_err_empty = False
        if len(self.disp) == 1 and np.isnan(self.disp[0]):
            self.disp_empty = True
        else:
            self.disp_empty = False
        if len(self.disp_err) == 1 and np.isnan(self.disp_err[0]):
            self.disp_err_empty = True
        else:
            self.disp_err_empty = False

        if not self.flux_empty and not self.error_empty:
            self.SNR = flux / error

        self.check_shapes()

    @staticmethod
    def check_shape(argument, shape):
        if argument.shape != shape:
            if len(argument) == 1:
                return np.full(shape, float(argument))
            else:
                raise IndexError(f'Incompatible argument shapes')
        else:
            return argument

    def check_shapes(self):
        shape = self.flux.shape

        if not self.error_empty:
            self.error = self.check_shape(self.error, shape)
        if not self.vel_empty:
            self.vel = self.check_shape(self.vel, shape)
        if not self.vel_err_empty:
            self.vel_err = self.check_shape(self.vel_err, shape)
        if not self.fwhm_empty:
            self.fwhm = self.check_shape(self.fwhm, shape)
        if not self.fwhm_err_empty:
            self.fwhm_err = self.check_shape(self.fwhm_err, shape)
        if not self.disp_empty:
            self.disp = self.check_shape(self.disp, shape)
        if not self.disp_err_empty:
            self.disp_err = self.check_shape(self.disp_err, shape)

    def subset(self, ind, inverse=False):
        subset = EmissionLine()

        if inverse:
            mask = np.ones(len(self.flux), np.bool)
            mask[ind] = 0
            ind = mask

        subset.name = self.name
        subset.wavelength = self.wavelength
        subset.flux = self.flux[ind]
        subset.flux_empty = False
        if not self.error_empty:
            subset.error = self.error[ind]
            subset.SNR = self.SNR[ind]
            subset.error_empty = False
        if not self.vel_empty:
            subset.vel = self.vel[ind]
            subset.vel_empty = False
        if not self.vel_err_empty:
            subset.vel_err = self.vel_err[ind]
            subset.vel_err_empty = False
        if not self.fwhm_empty:
            subset.fwhm = self.fwhm[ind]
            subset.fwhm_empty = False
        if not self.fwhm_err_empty:
            subset.fwhm_err = self.fwhm_err[ind]
            subset.fwhm_err_empty = False
        if not self.disp_empty:
            subset.disp = self.disp[ind]
            subset.disp_empty = False
        if not self.disp_err_empty:
            subset.disp_err = self.disp_err[ind]
            subset.disp_err_empty = False
        return subset

    def append(self, emission_line):
        result = EmissionLine()
        result.name = self.name
        result.wavelength = self.wavelength
        if not self.flux_empty and not emission_line.flux_empty:
            result.flux = np.append(self.flux, emission_line.flux)
            result.flux_empty = False
        if not self.error_empty and not emission_line.error_empty:
            result.error = np.append(self.error, emission_line.error)
            result.SNR = np.append(self.SNR, emission_line.SNR)
            result.error_empty = False
        if not self.vel_empty and not emission_line.vel_empty:
            result.vel = np.append(self.vel, emission_line.vel)
            result.vel_empty = False
        if not self.vel_err_empty and not emission_line.vel_err_empty:
            result.vel_err = np.append(self.vel_err, emission_line.vel_err)
            result.vel_err_empty = False
        if not self.fwhm_empty and not emission_line.fwhm_empty:
            result.fwhm = np.append(self.fwhm, emission_line.fwhm)
            result.fwhm_empty = False
        if not self.fwhm_err_empty and not emission_line.fwhm_err_empty:
            result.fwhm_err = np.append(self.fwhm_err, emission_line.fwhm_err)
            result.fwhm_err_empty = False
        if not self.disp_empty and not emission_line.disp_empty:
            result.disp = np.append(self.disp, emission_line.disp)
            result.disp_empty = False
        if not self.disp_err_empty and not emission_line.disp_err_empty:
            result.disp_err = np.append(self.disp_err, emission_line.disp_err)
            result.disp_err_empty = False
        return result
