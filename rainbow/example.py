import numpy as np
import matplotlib.pyplot as plt
import os
from rainbow import EmissionLine, EmissionLineSet, Spaxel
import rainbow.plot
import rainbow.fit
import rainbow.read
import rainbow.write

# ignore the stupid warning from the new matplotlib version
import matplotlib as mlp
import warnings

warnings.filterwarnings("ignore", category=mlp.cbook.MatplotlibDeprecationWarning)


def example():
    """
    An example which tests all the crucial functions of the package.
    For more detailed description of the steps check the jupyter notebook 'example.ipynb' from the example folder.
    """
    sf_basis_emission_lines = np.loadtxt(os.path.join(
        os.path.abspath(os.path.dirname(__file__)),
        '../example/R.txt'
    ), skiprows=1)

    sf_basis = EmissionLineSet()

    sf_basis.add_line(EmissionLine(
        name='Ha',
        flux=sf_basis_emission_lines[:, 0],
        error=sf_basis_emission_lines[:, 4]
    ))
    sf_basis.add_line(EmissionLine(
        name='Hb',
        flux=sf_basis_emission_lines[:, 1],
        error=sf_basis_emission_lines[:, 5]
    ))
    sf_basis.add_line(EmissionLine(
        name='O3',
        flux=sf_basis_emission_lines[:, 2],
        error=sf_basis_emission_lines[:, 6]
    ))
    sf_basis.add_line(EmissionLine(
        name='N2',
        flux=sf_basis_emission_lines[:, 3],
        error=sf_basis_emission_lines[:, 7]
    ))
    sf_basis.set_bpt_info()

    mixing_sequence_emission_line_set = rainbow.read.fits_table(os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                                                             '../example/AINBOW.fits'),
                                                                ['Halpha', 'Hbeta', 'OIII5007', 'NII6583'])
    mixing_sequence_emission_line_set.set_bpt_info()

    agn_basis_index = np.where(np.log10(mixing_sequence_emission_line_set.BPT['OIII5007/Hbeta']) > 0.75)[0]
    agn_basis = mixing_sequence_emission_line_set.subset(agn_basis_index)
    to_fit = mixing_sequence_emission_line_set.subset(agn_basis_index, inverse=True)

    rainbow.plot.bpt(agn_basis, color='red', show=0)
    rainbow.plot.bpt(sf_basis, color='blueviolet', size=0, show=0)
    rainbow.plot.bpt(to_fit, color='green', size=0)

    fitted = rainbow.fit(to_fit, agn_basis, sf_basis)

    rainbow.plot.bpt(agn_basis, color='red', show=0)
    rainbow.plot.bpt(sf_basis, color='blueviolet', size=0, show=0)
    rainbow.plot.bpt(fitted, colorcode=fitted.sf_fraction,
                     cmap='rainbow_r', clim=(0., 1.),
                     clb=1, clb_label='SF fraction',
                     vmin=0., vmax=1.,
                     size=0
                     )
