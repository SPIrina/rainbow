import numpy as np
import astropy.io.fits as pf
from rainbow import EmissionLine, EmissionLineSet, Spaxel

TABLE_NAME_SPACE = {
    'x_column_name': ['x_cor', 'x_cor1'],
    'y_column_name': ['y_cor', 'y_cor1'],
    'flux_suffix': ['_flux'],
    'flux_error_suffix': ['_flux_err', '_eflux'],
    'velocity_suffix': ['_vel'],
    'velocity_error_suffix': ['_vel_err'],
    'fwhm_suffix': ['_fwhm'],
    'fwhm_error_suffix': ['_fwhm_err'],
    'disp_suffix': ['_disp'],
    'disp_error_suffix': ['_disp_err'],
}


def _define_namespace(table_name_space_key, table_columns_names, suffix=True, column_name=''):
    if suffix:
        suffix = ''.join(
            [suffix if column_name + suffix in table_columns_names else ''
             for suffix in TABLE_NAME_SPACE[table_name_space_key]]
        )
        if suffix == '':
            suffix = None
        return suffix
    else:
        column_name = ''.join(
            [column_name if column_name in table_columns_names else ''
             for column_name in TABLE_NAME_SPACE[table_name_space_key]]
        )
        if column_name == '':
            column_name = None
        return column_name


def fits_table(
        table_path,
        line_list,
        ext=1,
        binning=np.nan,
        x_column_name='',
        y_column_name='',
        flux_suffix='',
        flux_error_suffix='',
        velocity_suffix='',
        velocity_error_suffix='',
        fwhm_suffix='',
        fwhm_error_suffix='',
        disp_suffix='',
        disp_error_suffix=''
):
    """

    Args:
        table_path: path to the FITS table
        line_list: list of the emission line names *as in the table columns*,
                   for example ['Halpha', 'Hbeta', 'OIII5007', 'NII6583']
        ext: FITS table extension
        binning: binning attribute in Spaxel class;
                 if nan (default), x & y are not read and the output is an EmissionLineSet;
                 if integer - the output is a Spaxel class instance.

        The following arguments have their defaults in the TABLE_NAME_SPACE dictionary;
        the dictionary can be updated if needed.
        x_column_name:
        y_column_name:
        flux_suffix:
        flux_error_suffix:
        velocity_suffix:
        velocity_error_suffix:
        fwhm_suffix:
        fwhm_error_suffix:
        disp_suffix:
        disp_error_suffix:

    Returns:
        EmissionLineSet object if no binning is specified;
        Spaxel object if binning is not np.nan (binning should be integer)

    """
    with pf.open(table_path) as table:
        table_columns_names = table[ext].columns.names
        if np.isnan(binning):
            read_object = EmissionLineSet()
        else:
            if x_column_name == '':
                x_column_name = _define_namespace('x_column_name', table_columns_names, suffix=False)
            x = table[ext].data[x_column_name]
            if y_column_name == '':
                y_column_name = _define_namespace('y_column_name', table_columns_names, suffix=False)
            y = table[ext].data[y_column_name]
            read_object = Spaxel(x=x,
                                 y=y,
                                 binning=binning)
        for line_name in line_list:
            if flux_suffix == '':
                flux_suffix = _define_namespace('flux_suffix', table_columns_names, column_name=line_name)
            if flux_suffix is None:
                flux = np.array([np.nan])
            else:
                flux = table[ext].data[line_name + flux_suffix]

            if flux_error_suffix == '':
                flux_error_suffix = _define_namespace('flux_error_suffix', table_columns_names, column_name=line_name)
            if flux_error_suffix is None:
                error = np.array([np.nan])
            else:
                error = table[ext].data[line_name + flux_error_suffix]

            if velocity_suffix == '':
                velocity_suffix = _define_namespace('velocity_suffix', table_columns_names, column_name=line_name)
            if velocity_suffix is None:
                vel = np.array([np.nan])
            else:
                vel = table[ext].data[line_name + velocity_suffix]

            if velocity_error_suffix == '':
                velocity_error_suffix = _define_namespace('velocity_error_suffix', table_columns_names, column_name=line_name)
            if velocity_error_suffix is None:
                vel_err = np.array([np.nan])
            else:
                vel_err = table[ext].data[line_name + velocity_error_suffix]

            if fwhm_suffix == '':
                fwhm_suffix = _define_namespace('fwhm_suffix', table_columns_names, column_name=line_name)
            if fwhm_suffix is None:
                fwhm = np.array([np.nan])
            else:
                fwhm = table[ext].data[line_name + fwhm_suffix]

            if fwhm_error_suffix == '':
                fwhm_error_suffix = _define_namespace('fwhm_error_suffix', table_columns_names, column_name=line_name)
            if fwhm_error_suffix is None:
                fwhm_err = np.array([np.nan])
            else:
                fwhm_err = table[ext].data[line_name + fwhm_error_suffix]

            if disp_suffix == '':
                disp_suffix = _define_namespace('disp_suffix', table_columns_names, column_name=line_name)
            if disp_suffix is None:
                disp = np.array([np.nan])
            else:
                disp = table[ext].data[line_name + disp_suffix]

            if disp_error_suffix == '':
                disp_error_suffix = _define_namespace('disp_error_suffix', table_columns_names, column_name=line_name)
            if disp_error_suffix is None:
                disp_err = np.array([np.nan])
            else:
                disp_err = table[ext].data[line_name + disp_error_suffix]

            read_object.add_line(EmissionLine(flux=flux,
                                              error=error,
                                              vel=vel,
                                              vel_err=vel_err,
                                              fwhm=fwhm,
                                              fwhm_err=fwhm_err,
                                              disp=disp,
                                              disp_err=disp_err,
                                              name=line_name))
    return read_object
