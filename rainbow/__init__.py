"""
Rainbow - a package to decompose optical emission line fluxes into two linear components.
MCMC based fitting allows to use multiple basis spectra to define a component. The algorithm is used to compute
star formation rate in AGN host galaxies.

© Irina Smirnova-Pinchukova https://spirina.gitlab.io/
"""
from rainbow.emission_line import EmissionLine
from rainbow.emission_line_set import EmissionLineSet
from rainbow.spaxel import Spaxel
from rainbow.run import run
from rainbow.fit import fit
from rainbow import read, plot, write
