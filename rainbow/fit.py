import numpy as np
import emcee
import multiprocessing
import tqdm.autonotebook as tqdm


def _lnprior(theta, bpt_distance, metallicity):
    if len(bpt_distance) > 1:
        sf, z, d, lnf = theta
        if d < bpt_distance.min() or d > bpt_distance.max():
            return -np.inf
    else:
        sf, z, lnf = theta
    if z < metallicity.min() or z > metallicity.max():
        return -np.inf
    if sf < 0. or sf > 1.:
        return -np.inf
    if lnf < -10.0 or lnf > 1.0:
        return -np.inf
    return 0.


def _lnlike(theta, xagn, xsf, y, erry, bpt_distance, metallicity):
    if len(bpt_distance) > 1:
        sf, z, d, lnf = theta
        xagnind = np.argmin(abs(bpt_distance - d))
        xsfind = np.argmin(abs(metallicity - z))
        model = xagn[xagnind] * (1. - sf) + xsf[xsfind] * sf
    else:
        sf, z, lnf = theta
        xsfind = np.argmin(abs(metallicity - z))
        model = xagn[0] * (1. - sf) + xsf[xsfind] * sf
    # sigma2 = erry ** 2.
    # return -0.5 * np.sum((y - model) ** 2. / sigma2 + np.log(2. * np.pi * sigma2))  # chi square function
    sigma2 = erry ** 2. + model ** 2 * np.exp(2 * lnf)
    return -0.5 * np.sum((y - model) ** 2. / sigma2 + np.log(2. * np.pi * sigma2))  # likelihood function


def _lnprob(theta, xagn, xsf, y, erry, bpt_distance, metallicity):
    lp = _lnprior(theta, bpt_distance, metallicity)
    if np.isinf(lp):
        return -np.inf
    return lp + _lnlike(theta, xagn, xsf, y, erry, bpt_distance, metallicity)


def _mcmc(fit_input):
    (y, erry,
     xagn, xsf,
     p0,
     metallicity, bpt_distance,
     nwalkers, ndim,
     steps, burnt_in_after) = fit_input

    sampler = emcee.EnsembleSampler(nwalkers, ndim, _lnprob,
                                    args=(xagn, xsf,
                                          y, erry,
                                          bpt_distance,
                                          metallicity)
                                    )
    sampler.run_mcmc(p0, steps)
    samples = sampler.flatchain[burnt_in_after:, :]
    # check on real data
    # labels = ['SF fraction', 'metallicity', 'BPT distance', 'lnf']
    # corner.corner(samples, quantiles=(0.16, 0.84), show_titles=True, labels=labels)
    # plt.show()

    sf = samples[:, 0].mean()
    sf_error = (np.quantile(samples[:, 0], 0.84) - np.quantile(samples[:, 0], 0.16)) / 2.
    z = samples[:, 1].mean()
    z_error = (np.quantile(samples[:, 1], 0.84) - np.quantile(samples[:, 1], 0.16)) / 2.

    return sf, sf_error, z, z_error


def fit(
        to_fit,
        agn_basis,
        sf_basis,
        nwalkers=100, steps=300, burnt_in_after=150,
        proc_number=None,
        normilize=True,
):
    """
    Args:
        to_fit: Spaxel or EmissionLineSet class instance
        agn_basis: same
        sf_basis: same

        arguments for mcmc; reed emcee package documentation for details
        https://emcee.readthedocs.io/en/stable/
        nwalkers:
        steps:
        burnt_in_after:

        proc_number: number of processers to be used by multiprocessing.Pool;
                     default is all the available processers minus one.
        normilize: bool; if True the emission lines are normalised by either Halpha or Hbeta;
                   this makes the process more stable to atthenuation uncertainties.

    Returns:
        the copy of 'to_fit' object, but with the fitted SF fraction and error information inside.

    """
    line_names = list(to_fit.emission_lines.keys())

    agn_number = len(agn_basis.emission_lines[line_names[0]].flux)
    sf_number = len(sf_basis.emission_lines[line_names[0]].flux)
    if agn_number == 0 or sf_number == 0:
        raise ValueError('AGN basis or SF basis has no data', agn_number, sf_number)

    bpt_distance = np.log10(agn_basis.BPT['NII6583/Halpha'])

    metallicity = 8.533 - 0.214 * np.log10(sf_basis.emission_lines['OIII5007'].flux /
                                           sf_basis.emission_lines['NII6583'].flux * 2.86)

    if normilize:
        normalization_line = {
            'OIII5007': 'Hbeta',
            'NII6583': 'Halpha',
            'SII6717': 'Halpha',
        }
        line_names.remove('Halpha')
        line_names.remove('Hbeta')
        xagn = np.array(
            [agn_basis.emission_lines[line_name].flux /
             agn_basis.emission_lines[normalization_line[line_name]].flux for line_name in line_names], dtype=float
        ).T
        xsf = np.array(
            [sf_basis.emission_lines[line_name].flux /
             sf_basis.emission_lines[normalization_line[line_name]].flux for line_name in line_names], dtype=float
        ).T

        y_array = np.array(
            [to_fit.emission_lines[line_name].flux /
             to_fit.emission_lines[normalization_line[line_name]].flux for line_name in line_names],
            dtype=float).T
        erry_array = np.array(
            [to_fit.emission_lines[line_name].error /
             to_fit.emission_lines[normalization_line[line_name]].flux for line_name in line_names],
            dtype=float).T
    else:
        xagn = np.array(
            [agn_basis.emission_lines[line_name].flux for line_name in line_names]
        ).T
        xsf = np.array(
            [sf_basis.emission_lines[line_name].flux for line_name in line_names]
        ).T

        y_array = np.array(
            [to_fit.emission_lines[line_name].flux for line_name in line_names]
        ).T
        erry_array = np.array(
            [to_fit.emission_lines[line_name].error for line_name in line_names]
        ).T

    if np.isnan(y_array).sum() > 0:
        raise ValueError('to_fit contains NaNs')

    if agn_number == 1:
        ndim = 3
        p0 = [np.array(
            [np.random.rand(),
             metallicity[np.random.randint(sf_number)],
             np.random.uniform(-10., 1., 1)]
        ) for _ in range(nwalkers)]
    else:
        ndim = 4
        p0 = [np.array(
            [np.random.rand(),
             metallicity[np.random.randint(sf_number)],
             bpt_distance[np.random.randint(agn_number)],
             np.random.uniform(-10., 1., 1)]
        ) for _ in range(nwalkers)]

    metallicity = np.array(metallicity, dtype=float)
    bpt_distance = np.array(bpt_distance, dtype=float)

    fit_input = [
        (y, erry,
         xagn, xsf,
         p0,
         metallicity, bpt_distance,
         nwalkers, ndim,
         steps, burnt_in_after)
        for (y, erry) in zip(y_array, erry_array)]

    if proc_number is None:
        proc_number = multiprocessing.cpu_count() - 1
    with multiprocessing.Pool(proc_number) as pool:
        arr = np.array(list(tqdm.tqdm(pool.imap(_mcmc, fit_input), total=len(fit_input)))).T

    sf, sf_error, z, z_error = arr[0], arr[1], arr[2], arr[3]
    to_fit.add_fitting_result(sf, sf_error,)

    return to_fit
