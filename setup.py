from setuptools import setup

setup(
    name='rainbow',
    version='2.3',
    packages=['rainbow'],
    scripts=['bin/rainbow'],
    # data_files=('example', ['example/R.txt', 'example/AINBOW.fits']),
    url='https://gitlab.com/SPIrina/rainbow',
    license='MIT',
    author='Irina Smirnova-Pinchukova',
    author_email='irinasmirnovapinchukova@gmail.com',
    description='Rainbow is a python package for AGN host galaxies BPT analysis.',
    # to install a specific package version use
    # conda install -c conda-forge numpy=1.15.4
    # conda install -c conda-forge numpy-base=1.15.4
    install_requires=[
        'numpy>=1.15.4,<1.16',  # if not 1.15.4, there will be an error
                                # "TypeError: ufunc 'isinf' not supported for the input types" in the fit function
                                # which I do not know how to solve
        'astropy>=3.1, <=3.2.3',
        'tqdm>=4.28.1',
        'matplotlib>=3.0.2',
        'emcee>=2.2.1'
    ]
)
