# Rainbow

This is a python package for AGN vs. SF BPT analysis.

Check the structure of the code here: https://spirina.gitlab.io/rainbow/
# Installation
```
python setup.py develop
```
To check if the installation was succesful run the following line from any folder:
```
rainbow --example
```

# How to run
The code takes arguments from a logfile, which path should be specified agter the --logfile keyword.
The data should be stored as pickled objects before using the following commands (see an example). 
```
rainbow --logfile <path to logfile>
rainbow -l <path to logfile>
```
Use the additional --status (-s) and --result (-r) keywords to show matplotlib plots:
```
rainbow --logfile <path to logfile> -sr
```

## Logfile
The mandatory arguments in the logfile are:
```
datadir <data directory path>
inputfile <inputfile name in datadir>
```
The additional arguments are:
```
outputdir <output directory>, if not present datadir is taken
```
To set AGN basis:
```
agn_min_bpt_x <defailt -0.4>
agn_max_bpt_y <defailt inf>
agn_min_bpt_y <defailt 0.75>
```
Spatial maps will be plotted if these arguments are specified:
```
nx <number of spatial pixels along X axis>
ny <number of spatial pixels along Y axis>
cdelt <arcseconds per spatial pixel>
```
With this basic spatial information the other features might be used:
```
xc <X axis pixel of the center>, if not present nx / 2. is taken
yc <Y axis pixel of the center>, if not present ny / 2. is taken
maximum_radius <radius in arcsec>, exclude spaxels outside the circle from the analysis/plotting/calculations
dividing_radius <radius in arcsec>, exclude spaxels outside the circle from the fitting
datacube <filename of the datacube>, overplots the whitelight image contours on the maps
```
